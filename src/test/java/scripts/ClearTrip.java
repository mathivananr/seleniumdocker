package scripts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.ClearTrip_Page;
import pages.TestScriptHandler;


@Listeners(pages.Listeners.class)
public class ClearTrip extends TestScriptHandler {
	
	ClearTrip_Page ClearTrip = new ClearTrip_Page();
	
	
	@Test(priority = 1)
	public void verify_Create_Round_Trip() {
		try {
			//maximize_Window_LaunchURL(getPropertyFile("URL"));
			driver.get("http://www.google.com");
			   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			   WebElement element = driver.findElement(By.name("q"));
			   element.sendKeys("Java");
			   //element.submit();
			   Thread.sleep(30000);
			
				/*
				 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
				 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
				 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
				 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
				 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
				 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
				 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
				 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
				 */	System.out.println("end");
		}
		catch(Exception e){
			Reporter.log("Error in method"+ e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	@Test(priority = 1)
	public void testCase2() {
		try {
			//maximize_Window_LaunchURL(getPropertyFile("URL"));
			driver.get("http://www.google.com");
			   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			   WebElement element = driver.findElement(By.name("q"));
			   element.sendKeys("Selenium");
			   //element.submit();
			
				/*
				 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
				 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
				 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
				 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
				 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
				 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
				 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
				 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
				 */	System.out.println("end");
		}
		catch(Exception e){
			Reporter.log("Error in method"+ e.getMessage());
			e.printStackTrace();
		}
	}
		
		@Test(priority = 1)
		public void testCase3() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Java");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		@Test(priority = 1)
		public void testCase4() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
	
	
	@Test(priority = 1)
	public void testCase5() {
		try {
			//maximize_Window_LaunchURL(getPropertyFile("URL"));
			driver.get("http://www.google.com");
			   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			   WebElement element = driver.findElement(By.name("q"));
			   element.sendKeys("Selenium");
			   //element.submit();
			
				/*
				 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
				 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
				 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
				 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
				 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
				 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
				 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
				 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
				 */	System.out.println("end");
		}
		catch(Exception e){
			Reporter.log("Error in method"+ e.getMessage());
			e.printStackTrace();
		}
	}
		
		@Test(priority = 1)
		public void testCase6() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Java");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		@Test(priority = 1)
		public void testCase7() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
	
	@Test(priority = 1)
	public void testCase8() {
		try {
			//maximize_Window_LaunchURL(getPropertyFile("URL"));
			driver.get("http://www.google.com");
			   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			   WebElement element = driver.findElement(By.name("q"));
			   element.sendKeys("Selenium");
			   //element.submit();
			
				/*
				 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
				 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
				 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
				 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
				 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
				 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
				 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
				 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
				 */	System.out.println("end");
		}
		catch(Exception e){
			Reporter.log("Error in method"+ e.getMessage());
			e.printStackTrace();
		}
	}
		
		@Test(priority = 1)
		public void testCase9() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Java");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		@Test(priority = 1)
		public void testCase10() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}


@Test(priority = 1)
	public void tc1_testCase11() {
		try {
			//maximize_Window_LaunchURL(getPropertyFile("URL"));
			driver.get("http://www.google.com");
			   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
			   WebElement element = driver.findElement(By.name("q"));
			   element.sendKeys("Selenium");
			   //element.submit();
			
				/*
				 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
				 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
				 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
				 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
				 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
				 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
				 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
				 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
				 */	System.out.println("end");
		}
		catch(Exception e){
			Reporter.log("Error in method"+ e.getMessage());
			e.printStackTrace();
		}
	}
		
		@Test(priority = 1)
		public void tc1_testCase12() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Java");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		@Test(priority = 1)
		public void tc1_testCase13() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase14() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase15() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase16() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase17() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase18() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase19() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase20() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase21() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase22() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase23() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase24() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase25() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase26() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase27() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase28() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase29() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase30() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase31() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase32() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase33() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase34() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase35() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase36() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase37() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase38() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase39() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase40() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase41() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase42() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase43() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase44() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase45() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase46() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase47() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase48() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				   
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase49() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				   
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}
		
		@Test(priority = 1)
		public void tc1_testCase50() {
			try {
				//maximize_Window_LaunchURL(getPropertyFile("URL"));
				driver.get("http://www.google.com");
				   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
				   WebElement element = driver.findElement(By.name("q"));
				   element.sendKeys("Selenium");
				   //element.submit();
				   
					/*
					 * ClearTrip.verify_Page_ClearTrip(); ClearTrip.click_RadioButton_RoundTrip();
					 * ClearTrip.select_DropDown_From(getPropertyFile("from_Destination"));
					 * ClearTrip.select_DropDown_To(getPropertyFile("to_Destination"));
					 * ClearTrip.select_DropDown_DepartOn_ReturnOn();
					 * ClearTrip.select_DropDown_Adults(getPropertyFile("adults"));
					 * ClearTrip.select_DropDown_Children(getPropertyFile("children"));
					 * ClearTrip.select_DropDown_Infants(getPropertyFile("infant"));
					 * ClearTrip.click_SearchFlight_Button(); ClearTrip.click_Button_Book();
					 */	System.out.println("end");
			}
			catch(Exception e){
				Reporter.log("Error in method"+ e.getMessage());
				e.printStackTrace();
			}
	}



}
