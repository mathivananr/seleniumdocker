package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.TestScriptHandler;



public class Edurekha extends TestScriptHandler{
	
	
@Test( priority = 0)
public void EdurekaProfile() throws InterruptedException{
	
	try {
		
		maximize_Window_LaunchURL(getPropertyFile("URL_Edurekha"));
	
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li//span[contains(text(),'Log In')])[1]")));
driver.findElement(By.xpath("(//li//span[contains(text(),'Log In')])[1]")).click();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_email")));
driver.findElement(By.id("si_popup_email")).click();
driver.findElement(By.id("si_popup_email")).clear();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_passwd")));
driver.findElement(By.id("si_popup_passwd")).click();
driver.findElement(By.id("si_popup_passwd")).clear();

Actions actions = new Actions(driver);
actions.moveToElement(driver.findElement(By.id("si_popup_email")));
actions.click();


actions.sendKeys("premcrescent@gmail.com");
actions.build().perform();
actions.moveToElement(driver.findElement(By.id("si_popup_passwd")));
actions.click();


actions.sendKeys("Pass1234");
actions.build().perform();
actions.moveToElement(driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']")));
actions.click();
actions.build().perform();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")));
driver.findElement(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//a[contains(text(),'My Profile')]")).click();


wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")));
driver.findElement(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fullName")));
driver.findElement(By.id("fullName")).sendKeys("Edureka");


driver.navigate().to("https://learning.edureka.co/my-profile");

Thread.sleep(5000);

driver.navigate().to("https://learning.edureka.co/onboarding/careerinterests");

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='interestedJob']")));
Select dropdownCurrentJob = new Select(driver.findElement(By.xpath("//select[@name='interestedJob']")));
dropdownCurrentJob.selectByVisibleText("Software Testing");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='elementType']")));
Select dropdownEmployementType = new Select(driver.findElement(By.xpath("//select[@name='elementType']")));
dropdownEmployementType.selectByVisibleText("Both");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='lastDrawnSalary']")));
Select dropdownCTC = new Select(driver.findElement(By.xpath("//select[@name='lastDrawnSalary']")));
dropdownCTC.selectByVisibleText("Not applicable");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Yes')]")));
driver.findElement(By.xpath("//label[contains(text(),'Yes')]")).click();
driver.findElement(By.name("preferredCity")).sendKeys("Mumbai");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit']")));
driver.findElement(By.xpath("//button[@type='submit']")).click();
driver.navigate().to("https://learning.edureka.co/");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user_name']")));
driver.findElement(By.xpath("//span[@class='user_name']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Log Out')]")));
driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
	}catch(Exception e) {
		
		e.printStackTrace();
		Assert.fail(e.getMessage());
		
		
	}
}

@Test( priority = 3)
public void EdurekaProfile1() throws InterruptedException{
	
	try {
		
		maximize_Window_LaunchURL(getPropertyFile("URL_Edurekha"));
	
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li//span[contains(text(),'Log In')])[1]")));
driver.findElement(By.xpath("(//li//span[contains(text(),'Log In')])[1]")).click();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_email")));
driver.findElement(By.id("si_popup_email")).click();
driver.findElement(By.id("si_popup_email")).clear();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_passwd")));
driver.findElement(By.id("si_popup_passwd")).click();
driver.findElement(By.id("si_popup_passwd")).clear();

Actions actions = new Actions(driver);
actions.moveToElement(driver.findElement(By.id("si_popup_email")));
actions.click();


actions.sendKeys("premcrescent@gmail.com");
actions.build().perform();
actions.moveToElement(driver.findElement(By.id("si_popup_passwd")));
actions.click();


actions.sendKeys("Pass1234");
actions.build().perform();
actions.moveToElement(driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']")));
actions.click();
actions.build().perform();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")));
driver.findElement(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//a[contains(text(),'My Profile')]")).click();


wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")));
driver.findElement(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fullName")));
driver.findElement(By.id("fullName")).sendKeys("Edureka1");


driver.navigate().to("https://learning.edureka.co/my-profile");
Thread.sleep(5000);
driver.navigate().to("https://learning.edureka.co/onboarding/careerinterests");

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='interestedJob']")));
Select dropdownCurrentJob = new Select(driver.findElement(By.xpath("//select[@name='interestedJob']")));
dropdownCurrentJob.selectByVisibleText("Software Testing");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='elementType']")));
Select dropdownEmployementType = new Select(driver.findElement(By.xpath("//select[@name='elementType']")));
dropdownEmployementType.selectByVisibleText("Both");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='lastDrawnSalary']")));
Select dropdownCTC = new Select(driver.findElement(By.xpath("//select[@name='lastDrawnSalary']")));
dropdownCTC.selectByVisibleText("Not applicable");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Yes')]")));
driver.findElement(By.xpath("//label[contains(text(),'Yes')]")).click();
driver.findElement(By.name("preferredCity")).sendKeys("Mumbai");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit']")));
driver.findElement(By.xpath("//button[@type='submit']")).click();
driver.navigate().to("https://learning.edureka.co/");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user_name']")));
driver.findElement(By.xpath("//span[@class='user_name']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Log Out')]")));
driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
	}catch(Exception e) {
		
		e.printStackTrace();
		Assert.fail(e.getMessage());
		
		
	}
}

@Test( priority = 2)
public void EdurekaProfile2() throws InterruptedException{
	
	try {
		
		maximize_Window_LaunchURL(getPropertyFile("URL_Edurekha"));
	

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li//span[contains(text(),'Log In')])[1]")));
driver.findElement(By.xpath("(//li//span[contains(text(),'Log In')])[1]")).click();


wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_email")));
driver.findElement(By.id("si_popup_email")).click();
driver.findElement(By.id("si_popup_email")).clear();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_passwd")));
driver.findElement(By.id("si_popup_passwd")).click();
driver.findElement(By.id("si_popup_passwd")).clear();

Actions actions = new Actions(driver);
actions.moveToElement(driver.findElement(By.id("si_popup_email")));
actions.click();


actions.sendKeys("premcrescent@gmail.com");
actions.build().perform();
actions.moveToElement(driver.findElement(By.id("si_popup_passwd")));
actions.click();


actions.sendKeys("Pass1234");
actions.build().perform();
actions.moveToElement(driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']")));
actions.click();
actions.build().perform();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")));
driver.findElement(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//a[contains(text(),'My Profile')]")).click();


wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")));
driver.findElement(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fullName")));
driver.findElement(By.id("fullName")).sendKeys("Edureka3");


driver.navigate().to("https://learning.edureka.co/my-profile");
Thread.sleep(5000);
driver.navigate().to("https://learning.edureka.co/onboarding/careerinterests");

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='interestedJob']")));
Select dropdownCurrentJob = new Select(driver.findElement(By.xpath("//select[@name='interestedJob']")));
dropdownCurrentJob.selectByVisibleText("Software Testing");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='elementType']")));
Select dropdownEmployementType = new Select(driver.findElement(By.xpath("//select[@name='elementType']")));
dropdownEmployementType.selectByVisibleText("Both");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='lastDrawnSalary']")));
Select dropdownCTC = new Select(driver.findElement(By.xpath("//select[@name='lastDrawnSalary']")));
dropdownCTC.selectByVisibleText("Not applicable");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Yes')]")));
driver.findElement(By.xpath("//label[contains(text(),'Yes')]")).click();
driver.findElement(By.name("preferredCity")).sendKeys("Mumbai");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit']")));
driver.findElement(By.xpath("//button[@type='submit']")).click();
driver.navigate().to("https://learning.edureka.co/");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user_name']")));
driver.findElement(By.xpath("//span[@class='user_name']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Log Out')]")));
driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
	}catch(Exception e) {
		
		e.printStackTrace();
		Assert.fail(e.getMessage());
		
		
	}
}

@Test( priority = 1)
public void EdurekaProfile4() throws InterruptedException{
	
	try {
		
		maximize_Window_LaunchURL(getPropertyFile("URL_Edurekha"));
	
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li//span[contains(text(),'Log In')])[1]")));
driver.findElement(By.xpath("(//li//span[contains(text(),'Log In')])[1]")).click();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_email")));
driver.findElement(By.id("si_popup_email")).click();
driver.findElement(By.id("si_popup_email")).clear();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("si_popup_passwd")));
driver.findElement(By.id("si_popup_passwd")).click();
driver.findElement(By.id("si_popup_passwd")).clear();

Actions actions = new Actions(driver);
actions.moveToElement(driver.findElement(By.id("si_popup_email")));
actions.click();


actions.sendKeys("premcrescent@gmail.com");
actions.build().perform();
actions.moveToElement(driver.findElement(By.id("si_popup_passwd")));
actions.click();


actions.sendKeys("Pass1234");
actions.build().perform();
actions.moveToElement(driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']")));
actions.click();
actions.build().perform();

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")));
driver.findElement(By.xpath("//a[@class='dropdown-toggle trackButton']//img[@class='img30']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//a[contains(text(),'My Profile')]")).click();


wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")));
driver.findElement(By.xpath("//li[@class='active']//a[@data-toggle='tab'][contains(text(),'My Profile')]")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")));
driver.findElement(By.xpath("//div[@class='personal-details']//i[@class='icon-pr-edit']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fullName")));
driver.findElement(By.id("fullName")).sendKeys("Edureka4");


driver.navigate().to("https://learning.edureka.co/my-profile");
Thread.sleep(5000);
driver.navigate().to("https://learning.edureka.co/onboarding/careerinterests");

wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='interestedJob']")));
Select dropdownCurrentJob = new Select(driver.findElement(By.xpath("//select[@name='interestedJob']")));
dropdownCurrentJob.selectByVisibleText("Software Testing");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='elementType']")));
Select dropdownEmployementType = new Select(driver.findElement(By.xpath("//select[@name='elementType']")));
dropdownEmployementType.selectByVisibleText("Both");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='lastDrawnSalary']")));
Select dropdownCTC = new Select(driver.findElement(By.xpath("//select[@name='lastDrawnSalary']")));
dropdownCTC.selectByVisibleText("Not applicable");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(),'Yes')]")));
driver.findElement(By.xpath("//label[contains(text(),'Yes')]")).click();
driver.findElement(By.name("preferredCity")).sendKeys("Mumbai");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit']")));
driver.findElement(By.xpath("//button[@type='submit']")).click();
driver.navigate().to("https://learning.edureka.co/");
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user_name']")));
driver.findElement(By.xpath("//span[@class='user_name']")).click();
wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Log Out')]")));
driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
	}catch(Exception e) {
		
		e.printStackTrace();
		Assert.fail(e.getMessage());
		
	}
}
}
