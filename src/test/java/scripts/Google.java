package scripts;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import pages.TestScriptHandler;

public class Google extends TestScriptHandler{

@Test
@Parameters({"LocalOrGrid","Browser"})
void search_Selenium(@Optional("local") String LocalOrGrid ,@Optional("chrome") String Browser) throws MalformedURLException {

try {

if(Browser.contentEquals("chrome")) {
if(LocalOrGrid.contentEquals("grid")) {
DesiredCapabilities capability = DesiredCapabilities.chrome();
driver = new RemoteWebDriver(new URL("http://3.137.163.85:4444/"), capability);
Reporter.log(" 'Chrome' browser launched");
}
if(LocalOrGrid.contentEquals("local")) {
WebDriverManager.chromedriver().setup();
driver = new ChromeDriver();
Reporter.log("'Chrome' browser launched");
}
}else if (Browser.contentEquals("internetexplorer")) {
if(LocalOrGrid.contentEquals("grid")) {
DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
driver = new RemoteWebDriver(new URL("http://3.137.163.85:4444/"), capability);
Reporter.log("'Internet Explore' browser launched");
}
if(LocalOrGrid.contentEquals("local")) {
WebDriverManager.chromedriver().setup();
driver = new InternetExplorerDriver();
Reporter.log(" 'Internet Explore' browser launched");
}
}
else {
Reporter.log("Browser name not matching");
}
   driver.get("http://www.google.com");
   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
   WebElement element = driver.findElement(By.name("q"));
   element.sendKeys("Selenium");
   element.submit();

   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.id("rso"))));

   List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));

   for (WebElement webElement : findElements)
   {
       System.out.println(webElement.getAttribute("href"));
   }

}catch(Exception e) {
e.printStackTrace();
Assert.fail(e.getMessage());
}
}

@Test
@Parameters({"LocalOrGrid","Browser"})
void search_Java(@Optional("local") String LocalOrGrid ,@Optional("chrome") String Browser) throws MalformedURLException {

try {

if(Browser.contentEquals("chrome")) {
if(LocalOrGrid.contentEquals("grid")) {
DesiredCapabilities capability = DesiredCapabilities.chrome();
driver = new RemoteWebDriver(new URL("http://3.137.163.85:4444/"), capability);
Reporter.log(" 'Chrome' browser launched");
}
if(LocalOrGrid.contentEquals("local")) {
WebDriverManager.chromedriver().setup();
driver = new ChromeDriver();
Reporter.log("'Chrome' browser launched");
}
}else if (Browser.contentEquals("internetexplorer")) {
if(LocalOrGrid.contentEquals("grid")) {
DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
driver = new RemoteWebDriver(new URL("http://3.137.163.85:4444/"), capability);
Reporter.log("'Internet Explore' browser launched");
}
if(LocalOrGrid.contentEquals("local")) {
WebDriverManager.chromedriver().setup();
driver = new InternetExplorerDriver();
Reporter.log(" 'Internet Explore' browser launched");
}
}
else {
Reporter.log("Browser name not matching");
}
   driver.get("http://www.google.com");
   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.name("q"))));
   WebElement element = driver.findElement(By.name("q"));
   element.sendKeys("Java");
   element.submit();

   (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOfElementLocated((By.id("rso"))));

   List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));

   for (WebElement webElement : findElements)
   {
       System.out.println(webElement.getAttribute("href"));
   }

}catch(Exception e) {
e.printStackTrace();
Assert.fail(e.getMessage());
}
}

}